kind: Template
apiVersion: v1
metadata:
  name: corenlp-template
  annotations:
    description: "Template for deploying the CoreNLP server"
labels:
  template: corenlp-template
  nlp-app: ${NLP_APP_NAME}
parameters:
- name: NAMESPACE
  description: Openshift project
  required: true
- name: NLP_APP_NAME
  description: Name of the CoreNLP application
  value: corenlp-default
- name: NLP_IMAGE_NAME
  description: Name of the CoreNLP image in the project
  value: odh-corenlp
- name: SOURCE_REPOSITORY_URL
  description: "Git repository url hosting the CoreNLP template"
  value: https://gitlab.com/opendatahub/ai-library.git
- name: SOURCE_REPOSITORY_REF
  description: "Git repository reference for the repo hosting the CoreNLP template"
  value: master
- name: CONTEXT_DIR
  description: "Relative directory path in the repo for the CoreNLP template files"
  value: "docker/sentiment_analysis/corenlp_server"
- name: NLP_SERVER_VERSION
  description: "Basename of the CoreNLP pre-compiled jars zip file available for download at https://stanfordnlp.github.io/CoreNLP/"
  value: stanford-corenlp-full-2018-02-27
- name: NLP_SERVER_PORT
  description: "The port for the pods CoreNLP service"
  value: "9000"
- name: NLP_SERVER_TIMEOUT
  description: "Timeout (seconds) for the CoreNLP server to wait for an annotation to finish before canceling it"
  value: "15000"
- name: NLP_SERVER_JAVA_HEAP_MEMORY
  description: "Maximum java heap size for the instance of the CoreNLP server. Appended to java's '-Xmx' argument"
  value: 4g
- name: NLP_RESOURCE_REQUESTS_CPU
  value: "300m"
- name: NLP_RESOURCE_REQUESTS_MEMORY
  value: "1000Mi"
- name: NLP_RESOURCE_LIMITS_CPU
  value: "1000m"
- name: NLP_RESOURCE_LIMITS_MEMORY
  value: "4000Mi"
objects:
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${NLP_IMAGE_NAME}
    namespace: ${NAMESPACE}
    labels:
      nlp-app: ${NLP_APP_NAME}
  spec:
    lookupPolicy:
      local: true
    tags:
    - name: latest
- kind: BuildConfig
  apiVersion: v1
  metadata:
    annotations:
      description: Defines how to build the application
    name: ${NLP_APP_NAME}
    namespace: ${NAMESPACE}
    template.alpha.openshift.io/wait-for-ready: "true"
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${NLP_IMAGE_NAME}:latest
    source:
      contextDir: ${CONTEXT_DIR}
      git:
        ref: ${SOURCE_REPOSITORY_REF}
        uri: ${SOURCE_REPOSITORY_URL}
      type: Git
    strategy:
      dockerStrategy:
        env:
        - name: NLP_SERVER_VERSION
          version: ${NLP_SERVER_VERSION}
        - name: NLP_SERVER_PORT
          value: ${NLP_SERVER_PORT}
        - name: NLP_SERVER_TIMEOUT
          value: ${NLP_SERVER_TIMEOUT}
        - name: NLP_SERVER_JAVA_HEAP_MEMORY
          value: ${NLP_SERVER_JAVA_HEAP_MEMORY}
      type: Docker
    triggers:
    - type: ConfigChange
    - imageChange: {}
      type: ImageChange
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      nlp-app: ${NLP_APP_NAME}
    name: ${NLP_APP_NAME}
    namespace: ${NAMESPACE}
  spec:
    replicas: 1
    selector:
      nlp-app: ${NLP_APP_NAME}
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Recreate
    template:
      metadata:
        labels:
          nlp-app: ${NLP_APP_NAME}
      spec:
        containers:
        - image: ${NLP_IMAGE_NAME}:latest
          name: corenlp-server
          namespace: ${NAMESPACE}
          resources:
            requests:
              cpu: ${NLP_RESOURCE_REQUESTS_CPU}
              memory: ${NLP_RESOURCE_REQUESTS_MEMORY}
            limits:
              cpu: ${NLP_RESOURCE_LIMITS_CPU}
              memory: ${NLP_RESOURCE_LIMITS_MEMORY}
          ports:
          - containerPort: ${{NLP_SERVER_PORT}}
            name: corenlp-server
            protocol: TCP
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
    test: false
    triggers:
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      nlp-app: ${NLP_APP_NAME}
    name: ${NLP_APP_NAME}
    namespace: ${NAMESPACE}
  spec:
    ports:
    - port: ${{NLP_SERVER_PORT}}
      protocol: TCP
      targetPort: ${{NLP_SERVER_PORT}}
    selector:
      nlp-app: ${NLP_APP_NAME}
    type: ClusterIP
