import model_topics as mt
import os
from os import listdir
from os.path import isfile, join
import argparse
import time
import json
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3
import tempfile

NUM_TOPICS = 2
NUM_WORDS = 10
PASSES = 50

class duplicate_train(object):

    def __init__(self):
        self.model = 'None'

    def predict(self,data,features_names):
        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["trainingdata", "outputmodel"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result
        s3Path = params['trainingdata']
        s3Destination = params['outputmodel']
        s3endpointUrl = os.environ['S3ENDPOINTURL']
        s3objectStoreLocation = os.environ['S3OBJECTSTORELOCATION']
        s3accessKey = os.environ['S3ACCESSKEY']
        s3secretKey = os.environ['S3SECRETKEY']


        # Create S3 session to access Ceph backend and get an S3 resource
        session = s3.create_session_and_resource(s3accessKey,
                                                 s3secretKey,
                                                 s3endpointUrl)
        objects = s3.get_objects(session, s3objectStoreLocation, s3Path)
        filelist = []

        # For each existing bug, perform topic modeling of the contents
        # to arrive at topics for each bug.

        tmpdir = str(tempfile.mkdtemp())
        for key in objects:
            print(key)
            doc_complete = []
            data = {}
            obj = session.Object(s3objectStoreLocation, key)
            contents = obj.get()['Body'].read().decode('utf-8')
            path = key.split("/")
            filename = path[-1]
            if contents:
                jcontents = json.loads(contents)
                title = jcontents['title']
                comments = jcontents['content']
                doc_complete = comments.split('\n')
                doc_clean = [mt.clean(doc).split() for doc in doc_complete]
                topics = mt.gen_topics(doc_clean, NUM_TOPICS, NUM_WORDS, PASSES)
                data['title'] = title
                data['content'] = topics
                filelist.append(filename)
                with open(tmpdir + "/" + filename, 'w') as outfile:
                    json.dump(data, outfile)

        s3.upload_folder(s3accessKey,
                         s3secretKey,
                         s3endpointUrl,
                         s3objectStoreLocation,
                         tmpdir,
                         s3Destination)
        return result


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = duplicate_train()
  obj.predict(data,20)
  
if __name__== "__main__":
  main()

